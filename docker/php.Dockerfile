FROM php:8.0-fpm-alpine

RUN apk add --no-cache \
    zip \
    libzip-dev \
    coreutils \
    gettext \
    php8-fpm \
    php8-ctype \
    php8-curl \
    php8-dom \
    php8-gd \
    php8-iconv \
    php8-json \
    php8-intl \
    php8-fileinfo\
    php8-mbstring \
    php8-opcache \
    php8-openssl \
    php8-pdo \
    php8-pdo_mysql \
    php8-mysqli \
    php8-xml \
    bash \
    php8-xsl \
    php8-zlib \
    php8-phar \
    php8-tokenizer \
    php8-session \
    php8-simplexml \
    php8-zip \
    libxml2-dev \
    php8-xmlwriter \
    php8-sodium \
    php8-sockets \
    php8-pecl-apcu \
    make \
    curl \
    nginx \
    supervisor \
    git \
    php8-ftp \
    openssl-dev \
    libpng-dev \
    libwebp-dev \
    php8-pecl-mongodb \
    && apk del \
    && curl -sS https://getcomposer.org/installer | php8 -- --install-dir=/usr/bin --filename=composer

RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && pecl install xdebug-3.0.0 \
    && pecl install mongodb \
#    && docker-php-ext-enable xdebug \
    && docker-php-ext-install sockets

RUN docker-php-ext-install soap

RUN docker-php-ext-configure zip
RUN docker-php-ext-install zip

RUN docker-php-ext-configure ftp
RUN docker-php-ext-install ftp

RUN apk add libpng-dev libwebp-dev libjpeg-turbo-dev freetype-dev && \
      docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp && \
      docker-php-ext-install gd

RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN apk add --no-cache tzdata
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime

RUN  echo 'pm.max_children = 100' >> /etc/php8/php-fpm.d/www.conf && \
     echo 'pm.start_servers = 30' >> /etc/php8/php-fpm.d/www.conf && \
     echo 'pm.min_spare_servers = 30' >> /etc/php8/php-fpm.d/www.conf && \
     echo 'pm.max_spare_servers = 50' >> /etc/php8/php-fpm.d/www.conf
#RUN curl "http://pecl.php.net/get/xhprof-2.3.2.tgz" -fsL -o ./xhprof-2.3.2.tgz && \
#    mkdir /var/xhprof && tar xf ./xhprof-2.3.2.tgz -C /var/xhprof && \
#    cd /var/xhprof/xhprof-2.3.2/extension && \
#    phpize && \
#    ./configure && \
#    make && \
#    make install


RUN #docker-php-ext-enable xhprof

#folder for xhprof profiles (same as in file xhprof.ini)
RUN mkdir -m 777 /profiles

#RUN echo "[XDebug]" >> /etc/php8/php.ini \
#    && echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20200930/xdebug.so" >> /etc/php8/php.ini \
#    && echo "xdebug.client_host=192.168.3.6" >> /etc/php8/php.ini \
#    && echo "xdebug.mode=debug" >> /etc/php8/php.ini \
#    && echo "xdebug.start_with_request=yes" >> /etc/php8/php.ini \
#    && echo "xdebug.idekey=PHPSTORM" >> /etc/php8/php.ini \
#    && echo "xdebug.client_port=9000" >> /etc/php8/php.ini


RUN echo "date.timezone = Europe/Moscow" >> /etc/php8/php.ini

RUN echo 'extension=/usr/lib/php8/modules/pdo_mysql.so'>> /etc/php8/php.ini


RUN chown -R root:root /etc/crontabs && chmod -R 0644 /etc/crontabs \
    && rm -rf /run/nginx \
    && mkdir /run/nginx \
    && mkdir /run/php \
    && echo "daemon off;" >> /etc/nginx/nginx.conf \
    && sed -i "s/display_errors = On/display_errors = Off/" /etc/php8/php.ini \
    && sed -i "s/post_max_size = 8M/post_max_size = 100M/" /etc/php8/php.ini \
    && sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 100M/" /etc/php8/php.ini \
    && sed -i "s/user = www-data/user = root/" /etc/php8/php-fpm.d/www.conf \
    && sed -i "s/group = www-data/group = root/" /etc/php8/php-fpm.d/www.conf \
# Supervisor conf
    && echo "[supervisord]" >> /etc/supervisord.conf \
    && echo "nodaemon = true" >> /etc/supervisord.conf \
    && echo "user = root" >> /etc/supervisord.conf \
    && echo "[program:php-fpm8]" >> /etc/supervisord.conf \
    && echo "command = /usr/sbin/php-fpm8 -FR" >> /etc/supervisord.conf \
    && echo "autostart = true" >> /etc/supervisord.conf \
    && echo "autorestart = true" >> /etc/supervisord.conf \
    && echo "[program:nginx]" >> /etc/supervisord.conf \
    && echo "command = /usr/sbin/nginx" >> /etc/supervisord.conf \
    && echo "autostart = true" >> /etc/supervisord.conf \
    && echo "autorestart = true" >> /etc/supervisord.conf \
    && echo "[program:crond]" >> /etc/supervisord.conf \
    && echo "command = crond -f" >> /etc/supervisord.conf \
    && echo "autostart = true" >> /etc/supervisord.conf \
    && echo "autorestart = true" >> /etc/supervisord.conf \
    && echo "[program:queue]" >> /etc/supervisord.conf \
    && echo "command = /usr/local/bin/php /var/www/html/artisan queue:listen" >> /etc/supervisord.conf \
    && echo "autostart = true" >> /etc/supervisord.conf \
    && echo "autorestart = true" >> /etc/supervisord.conf


CMD /usr/bin/supervisord 