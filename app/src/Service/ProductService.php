<?php

namespace App\Service;


use App\DTO\FindAllProductsDTO;
use App\Entity\Product;

class ProductService implements iProductService
{
    public function __construct(private \App\Repository\ProductRepository $productRepository){}

    public function getAllProducts(FindAllProductsDTO $productsDTO, int $productsPerPage): array
    {
        $paginationData = $this->calculatePage($productsDTO->page, $productsPerPage);
        //Демо вариант, на практике я бы сделал, пагинацию, как один из фильтров
        return $this->productRepository->findBy([],[], $paginationData['limit'], $paginationData['offset']);
    }

    private function calculatePage(int $page, $productsPerPage): array
    {

        $offset = ($productsPerPage * $page) - $productsPerPage;
        return [
            'limit'  => $productsPerPage,
            'offset' => $offset
        ];
    }

    public function getProductBySlug(string $slug): ?Product
    {
        return $this->productRepository->findOneBy(['slug' => $slug]);
    }
}