<?php

namespace App\Service;

use App\DTO\FindAllProductsDTO;
use App\Entity\Product;

interface iProductService
{
    public function getAllProducts(FindAllProductsDTO $productsDTO,int $productsPerPage): array;
    public function getProductBySlug(string $slug): ?Product;
}