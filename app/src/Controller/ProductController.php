<?php

namespace App\Controller;

use App\DTO\FindAllProductsDTO;
use App\Helper;
use App\Service\iProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProductController extends AbstractController
{
    #[Route('/products', name: 'app_product')]
    public function getAllProducts(iProductService $productService, Request $request): Response
    {

        $page = $request->query->get('page') ?? 1;
        $productDTO = new FindAllProductsDTO($page);
        $productsPerPage = $this->getParameter('products.products_per_page');
        $product = $productService->getAllProducts($productDTO, $productsPerPage);
        return $this->json(Helper::formResponse($product), Response::HTTP_NOT_FOUND,[], Helper::getCircularReferenceHandler());
    }

    #[Route('/product/{slug}', name: 'app_product_slug')]
    public function getProductBySlug(iProductService $productService, Request $request, string $slug): Response
    {
        $product = $productService->getProductBySlug($slug);
        if ($product === null){
            return $this->json(Helper::formResponse('Not found', Response::HTTP_NOT_FOUND, true), Response::HTTP_NOT_FOUND,[], Helper::getCircularReferenceHandler());
        }
        return $this->json(Helper::formResponse($product), Response::HTTP_NOT_FOUND,[], Helper::getCircularReferenceHandler());
    }

}
