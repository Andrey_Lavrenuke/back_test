<?php

namespace App;


use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class Helper
{
    public static function getCircularReferenceHandler(): array
    {
       return [ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER=>function ($obj){return $obj->getId();}];
    }

    public static function formResponse($data, $code = 200, $error = false): array
    {

        return [
            'error' => $error,
            'code'  => $code,
            'data'  => $data
        ];
    }
}