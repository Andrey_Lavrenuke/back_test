<?php

namespace App\Entity;

use App\Repository\AttributesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AttributesRepository::class)]
class Attributes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $attribute_name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $attribute_value = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $attribute_unit = null;

    #[ORM\ManyToOne(inversedBy: 'Attributes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $Product = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttributeName(): ?string
    {
        return $this->attribute_name;
    }

    public function setAttributeName(string $attribute_name): self
    {
        $this->attribute_name = $attribute_name;

        return $this;
    }

    public function getAttributeValue(): ?string
    {
        return $this->attribute_value;
    }

    public function setAttributeValue(?string $attribute_value): self
    {
        $this->attribute_value = $attribute_value;

        return $this;
    }

    public function getAttributeUnit(): ?string
    {
        return $this->attribute_unit;
    }

    public function setAttributeUnit(?string $attribute_unit): self
    {
        $this->attribute_unit = $attribute_unit;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(?Product $Product): self
    {
        $this->Product = $Product;

        return $this;
    }
}
