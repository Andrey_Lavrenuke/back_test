<?php

namespace App\DTO;


class FindAllProductsDTO implements DTO
{
    public function __construct(
        public int $page = 1
    ){}
}